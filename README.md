# Arcadbox - Firmware

Recette de construction de firmwares OpenWRT personnalisés intégrant les paquets et la configuration Arcadbox par défaut.

## Construire manuellement son firmware

```shell
# Exemple: construire un firmware OpenWRT

## Pour le routeur Linksys WRT1900AC
make OPENWRT_TARGET="mvebu/cortexa9" OPENWRT_PROFILE="linksys_wrt1900ac-v2" build

# Pour le routeur Linksys WRT1200AC
make OPENWRT_TARGET="mvebu/cortexa9" OPENWRT_PROFILE="linksys_wrt1200ac" build
```

## Téléchargements

### Version de développement

- [Dernière version de développement](https://gitlab.com/arcadbox/firmware/-/jobs/artifacts/develop/browse/bin/?job=OpenWRTFirmwares)