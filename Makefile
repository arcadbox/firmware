OPENWRT_VERSION ?= 21.02.1
OPENWRT_TARGET ?= mvebu/cortexa9
OPENWRT_TARGET_DASHED ?= $(shell echo $(OPENWRT_TARGET) | sed 's|/|-|')
OPENWRT_PROFILE ?= linksys_wrt1900ac-v2
OPENWRT_PACKAGES ?= arcadbox-device arcadbox-server arcadbox-gateway-client arcadbox-tunnel-client luci

GITLAB_TOKEN ?=
GITLAB_PROJECT_ID ?= 15257603
JOB_ARTIFACTS_BRANCH ?= develop
ARTIFACTS_JOB ?= OpenWRTPackages
ARCHIVE_ARCH ?= arm7

IMAGEBUILDER_URL ?= https://downloads.openwrt.org/releases/$(OPENWRT_VERSION)/targets/$(OPENWRT_TARGET)/openwrt-imagebuilder-$(OPENWRT_VERSION)-$(OPENWRT_TARGET_DASHED).Linux-x86_64.tar.xz

IMAGEBUILDER_ARCHIVE_PATH := tmp/imagebuilder-$(OPENWRT_VERSION)-$(OPENWRT_TARGET_DASHED).tar.xz
IMAGEBUILDER_DIR_PATH := imagebuilder/$(OPENWRT_VERSION)/$(OPENWRT_TARGET)
IMAGEBUILDER_CUSTOM_PACKAGES_DIR_PATH := $(IMAGEBUILDER_DIR_PATH)/packages

BIN_DIR := "$(shell readlink -f bin)/$(OPENWRT_VERSION)/$(OPENWRT_TARGET)/$(OPENWRT_PROFILE)"

include targets/*.mk

all:

docker-image:
	docker build \
		-t arcadbox-firmware:latest \
		./misc/ci

docker-build:
	docker run -it \
		--rm \
		-v "$(shell pwd):/project" \
		-v "$(shell readlink -f packages):/project/packages" \
		-w "/project" \
		arcadbox-firmware:latest

build: $(IMAGEBUILDER_DIR_PATH) $(IMAGEBUILDER_CUSTOM_PACKAGES_DIR_PATH)
	sed -i -n -e '/^src imagebuilder file:packages/!p' -e '$$asrc imagebuilder file:packages' "$(IMAGEBUILDER_DIR_PATH)/repositories.conf"
	mkdir -p "$(BIN_DIR)"
	$(MAKE) \
		-C "$(IMAGEBUILDER_DIR_PATH)" \
		PROFILE="$(OPENWRT_PROFILE)" \
		PACKAGES="$(OPENWRT_PACKAGES)" \
		BIN_DIR="$(BIN_DIR)" \
		clean image

$(IMAGEBUILDER_DIR_PATH): $(IMAGEBUILDER_ARCHIVE_PATH)
	mkdir -p "$(IMAGEBUILDER_DIR_PATH)"
	tar -xf "$(IMAGEBUILDER_ARCHIVE_PATH)" --strip-components 1 -C "$(IMAGEBUILDER_DIR_PATH)"

$(IMAGEBUILDER_ARCHIVE_PATH):
	mkdir -p $(shell dirname "$(IMAGEBUILDER_ARCHIVE_PATH)")
	wget -O "$(IMAGEBUILDER_ARCHIVE_PATH)" "$(IMAGEBUILDER_URL)"

$(IMAGEBUILDER_CUSTOM_PACKAGES_DIR_PATH):
	mkdir -p packages
	ln -s "$(shell readlink -f packages)" "$(IMAGEBUILDER_CUSTOM_PACKAGES_DIR_PATH)"

download-custom-packages:
	mkdir -p tmp
	mkdir -p packages
	curl -v -L --fail \
		-H "Private-Token:$(GITLAB_TOKEN)" \
		-o tmp/packages.zip \
		https://gitlab.com/api/v4/projects/$(GITLAB_PROJECT_ID)/jobs/artifacts/$(JOB_ARTIFACTS_BRANCH)/download?job=$(ARTIFACTS_JOB)
	cd tmp && unzip -j -o packages.zip -d ../packages
	rm tmp/packages.zip