all: linksys-wrtXXXXac

linksys-wrtXXXXac: linksys-wrt1200ac linksys-wrt1900ac

linksys-wrt1200ac:
	$(MAKE) OPENWRT_TARGET="mvebu/cortexa9" OPENWRT_PROFILE="linksys_wrt1200ac" build

linksys-wrt1900ac:
	$(MAKE) OPENWRT_TARGET="mvebu/cortexa9" OPENWRT_PROFILE="linksys_wrt1900ac-v2" build